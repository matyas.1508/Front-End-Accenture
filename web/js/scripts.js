const elementoCorreo = document.querySelector("#inputEmail");
const elementoConstraseña = document.querySelector("#inputPassword");
const elementoDireccion = document.querySelector("#inputAddress");
const elementoCiudad = document.querySelector("#inputCity");
const elementoEstado = document.querySelector("#inputState");
const elementoCP = document.querySelector("#inputZip");
const elementoMensaje = document.querySelector("#inputMessage");
const elementoImagen = document.querySelector("#inputImage");
const elementoAcepta = document.querySelector("#inputAccept");

/*console.log(inputEmail.value);
console.log(inputPassword.value);
console.log(inputAddress.value);
console.log(inputCity.value);
console.log(inputState.value);
console.log(inputZip.value);
console.log(inputMessage.value);
console.log(inputImage.value);
console.log(inputAccept.value);
console.log(elementoCorreo.value);*/


function validacion() {
    if ( !(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(elementoCorreo.value)) ) {
        alert("Por favor, ingrese una dirección de correo válida");
        return false;
    } else if ( elementoConstraseña.value == null || elementoConstraseña.value.length == 0 || /^\s+$/.test(elementoConstraseña.value) ) {
        alert("Por favor, ingrese una contraseña");
        return false;
    } else if (elementoConstraseña.value.length < 6 ) {
        alert("Por favor, ingrese una constraseña de 6 caracteres o más");
    } else if ( elementoDireccion.value == null || elementoDireccion.value.length == 0 || /^\s+$/.test(elementoDireccion.value) ) {
        alert("Por favor, ingrese una dirección");
        return false;
    } else if ( elementoCiudad.value == null || elementoCiudad.value.length == 0 || /^\s+$/.test(elementoCiudad.value) ) {
        alert("Por favor, ingrese una ciudad");
        return false;
    } else if ( elementoEstado.value == "" ) {
        alert("Por favor, seleccione alguno de los Estados listados");
        return false;
    } else if ( isNaN(elementoCP.value) || elementoCP.value == "" ) {
        alert("Por favor, ingrese su código postal, recuerde que sólo se aceptan números");
        return false;
    } else if ( elementoMensaje.value == null || elementoMensaje.value.length == 0 || /^\s+$/.test(elementoMensaje.value) ) {
        alert("Por favor, ingrese un mensaje");
        return false;
    /*} else if ( validaImagen() ) {
        alert("Error en la imagen");
        console.log("imagen");
        return false;*/
    } else if ( !elementoAcepta.checked ) {
        alert("Por favor, acepte enviar este formulario");
        return false;
    } else {
        return true;
    } 
}

