﻿Visitar Try Git!!

spacemonkey7.

https://github.com/karabaja4/vscode-explorer-git-status <--- Optimizar explorer VSC
C:\Program Files\Microsoft VS Code\resources\app\out\vs\workbench\workbench.main.js <--- agregar-> function injectGitFileStatus(){const e=document.getElementById("workbench.view.explorer");if(e){const t=e.getElementsByClassName("explorer-folders-view")[0];if(t){const e=t.getElementsByClassName("monaco-tree")[0];if(e){const t=e.getElementsByClassName("monaco-tree-row")[0];if(t){const e=t.getElementsByClassName("explorer-item")[0];if(e){const t=require("path"),r=require("child_process").exec,o=document.createElement("style");document.body.appendChild(o);const s=e=>"~"===e[0]?t.join(process.env.HOME,e.slice(1)):e,i=e=>{const t=process.env.HOME;if(t&&e.startsWith(t)){const r=new RegExp(`^${t}`);return e.replace(r,"~")}return e},n=e=>t.normalize(e.substr(3)).replace(/\\+$/,"").replace(/\/+$/,"").replace(/\\/g,"\\\\"),c=e=>{let r=1;const o=[],s=t.sep.replace(/\\/g,"\\\\");for(;r=e.indexOf(s,r)+1;)o.push(e.substr(0,r-1));return o},a=(e,r,o)=>`#workbench\\.view\\.explorer .explorer-folders-view .monaco-tree .monaco-tree-rows .monaco-tree-row .explorer-item[title="${i(t.join(e,r).replace(/\\/g,"\\\\"))}" i]{${o}}`,l=t.normalize(t.dirname(e.getAttribute("title"))),m={cwd:s(l)};return void r("git rev-parse --show-toplevel",m,(e,t,i)=>{if(!e){const e=t.trim(),i=()=>{const t={cwd:s(e)};r("git status --short --ignored",t,(t,r,s)=>{if(!t){const t=r.split("\n"),s=t.filter(e=>e.startsWith("?? ")).map(e=>n(e)),i=t.filter(e=>e.startsWith(" M ")).map(e=>n(e)),l=t.filter(e=>e.startsWith("!! ")).map(e=>n(e));let m="";const p=new Set,f=new Set;s.forEach(t=>{c(t).forEach(e=>{p.add(e)}),m+=a(e,t,`color:#32CD32;`)}),i.forEach(t=>{c(t).forEach(e=>{f.add(e)}),m+=a(e,t,`color:#FF8C00;`)}),l.forEach(t=>{m+=a(e,t,`opacity:0.4;`)}),p.forEach(t=>{m+=a(e,t,`color:#32CD32;`)}),f.forEach(t=>{m+=a(e,t,`color:#FF8C00;`)}),o.innerHTML!==m&&(o.innerHTML=m)}setTimeout(i,5e3)})};i()}})}}}}}setTimeout(injectGitFileStatus,5e3)}injectGitFileStatus();


Extensiones instaladas en casa:
    * Angular 2 TypeScript Emmet
    * ignore-gitignore
    * JSS Snippets
    * Live HTML Previewer
    * Sublime Text Keymap


http://code.goibela.com/javascript/comprobar-extension-de-archivo-en-javascript/
http://javascriptissexy.com/oop-in-javascript-what-you-need-to-know/
https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html

//
Documentación:
- HTML/CSS/JS: https://www.w3schools.com
- BEM (CSS): https://en.bem.info/
- SASS (CSS): http://sass-lang.com/
- TypeScript:
    - (Ver dentro de Handbook) - https://www.typescriptlang.org/docs
- Angular: https://angular.io/docs

Documentación Extra:
- Class en ES6: https://googlechrome.github.io/samples/classes-es6/
- Typescript by John Papa:
    - https://johnpapa.net/typescriptpost1/
    - https://johnpapa.net/typescriptpost2/
    - https://johnpapa.net/typescriptpost3/
    - https://johnpapa.net/typescriptpost4/

Links de Interes:
- https://fonts.google.com
- https://color.adobe.com
- http://www.htmlcolorcodes.com
- https://material.io/
- https://regexr.com
//

call apply bind <-- leer

https://codecraft.tv/courses/angular/routing/parameterised-routes/