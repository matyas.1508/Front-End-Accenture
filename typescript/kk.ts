// class Persona {
//     public nombre: string;
//     public apellido: string;
//     constructor(nombre: string, apellido: string){
//         this.nombre = nombre;  
//         this.apellido = apellido;  
//     }
// }

// let juan = new Persona("Juan","Pérez");

interface INombrable {
    nombre: string;
}

class Persona implements INombrable {
    nombre: string;
    saludar() : string {
        return `Hola, soy ${this.nombre}`
    }
}

function saludar (n: INombrable) : void {
    alert(`Hola soy ${n.nombre}`)
}