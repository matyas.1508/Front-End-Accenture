// class Persona {
//     public nombre: string;
//     public apellido: string;
//     constructor(nombre: string, apellido: string){
//         this.nombre = nombre;  
//         this.apellido = apellido;  
//     }
// }
var Persona = /** @class */ (function () {
    function Persona() {
    }
    Persona.prototype.saludar = function () {
        return "Hola, soy " + this.nombre;
    };
    return Persona;
}());
function saludar(n) {
    alert("Hola soy " + n.nombre);
}
