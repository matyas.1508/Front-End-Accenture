import { Persona } from "./Persona";
import { Soldado } from "./Soldado";

var personaJose = new Persona("José", 1.80, 27);
var soldadoLeon = new Soldado("Leon", 1.70, 25, "Cabo");

console.log(personaJose.obtenerEdad());
console.log(soldadoLeon.obtenerEdad());
console.log(Soldado);
