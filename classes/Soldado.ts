import { Persona } from "./Persona";

export class Soldado extends Persona {
    public rango: string;
    constructor(nombre: string, altura: number, edad: number, rango) {
        super(nombre, altura, edad);
        this.rango = rango;    
    }
    saludarRango() {
        return "Señor, sí, señor, mi rango es " + this.rango;
        // return this.edad;
        // return this.altura;
        // return this.nombre;
    }
}