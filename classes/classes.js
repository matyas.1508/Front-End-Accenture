"use strict";
exports.__esModule = true;
var Persona_1 = require("./Persona");
var Soldado_1 = require("./Soldado");
var personaJose = new Persona_1.Persona("José", 1.80, 27);
var soldadoLeon = new Soldado_1.Soldado("Leon", 1.70, 25, "Cabo");
console.log(personaJose.obtenerEdad());
console.log(soldadoLeon.saludarRango());
console.log(Soldado_1.Soldado);
