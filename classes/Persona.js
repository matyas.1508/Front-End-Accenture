"use strict";
exports.__esModule = true;
var Persona = /** @class */ (function () {
    function Persona(nombre, altura, edad) {
        this.nombre = nombre;
        this.altura = altura;
        this.edad = edad;
    }
    Persona.prototype.obtenerEdad = function () {
        return this.edad;
    };
    return Persona;
}());
exports.Persona = Persona;
