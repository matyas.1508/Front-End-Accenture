export class Persona {
    public nombre: string;
    public altura: number;
    public edad: number;
    constructor(nombre: string, altura: number, edad: number) {
        this.nombre = nombre;
        this.altura = altura;
        this.edad = edad;
    }
    obtenerEdad() {
        return this.edad;
    }
}