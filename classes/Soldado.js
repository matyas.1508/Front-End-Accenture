"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var Persona_1 = require("./Persona");
var Soldado = /** @class */ (function (_super) {
    __extends(Soldado, _super);
    function Soldado(nombre, altura, edad, rango) {
        var _this = _super.call(this, nombre, altura, edad) || this;
        _this.rango = rango;
        return _this;
    }
    Soldado.prototype.saludarRango = function () {
        return "Señor, sí, señor, mi rango es " + this.rango;
        // return this.edad;
        // return this.altura;
        // return this.nombre;
    };
    return Soldado;
}(Persona_1.Persona));
exports.Soldado = Soldado;
